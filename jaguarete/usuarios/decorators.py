from django.shortcuts import redirect, reverse, HttpResponse

# Agrego algun decorador propio del que no encontré implementacion
# nativa en Django.

# Este decorador fuerza a que el usuario del request NO este logueado, 
# seria un opuesto a @login_required. Lo quiero para limitar acceso a los
# sitios de login o registro cuando el usuario ya esta autenticado.
def not_logged_required(function):
    def wrapper(request, *args, **kwargs):
        if not request.user.is_authenticated:
            return function(request, *args, **kwargs)
        else:
            return redirect('/')
    
    # ejecuto wrapper.
    return wrapper
