from django.urls import path
from . import views

# En este archivo se definirán los linkeos a las views de la aplicación "Usuarios".
app_name = "usuarios"
urlpatterns = [
    path("login/", views.ingreso, name="ingreso"),
    path("salir/", views.salida, name="salida"),
    path("registro/", views.registro, name="registro"),
]
