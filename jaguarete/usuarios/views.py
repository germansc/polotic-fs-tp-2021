from django.contrib import auth
from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect, render, reverse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

from .forms import FormRegistro, FormIngreso
from .decorators import not_logged_required


# View de Registro de Usuario.
# Genero el form y lo presento al usuario y 
# recibo por Metodo POST lo que hayan completado.
# Si se valida, lo guardo y lo mando a logearse.
@not_logged_required
def registro(request):
    
    # Si el request es POST, llega con los datos.
    if request.method == 'POST':
        
        # Genero un form con los datos que llegaron.
        form = FormRegistro(request.POST)

        if form.is_valid():
            # Registro el usuario y redirijo a login.
            form.save()
            
            # Genero un mensaje para notificar en la proxima pantalla.
            uname = form.cleaned_data.get('username')
            messages.info(request, f"Usuario {uname.lower()} registrado!")

            # Redirijo a la pantalla de Ingreso.
            return redirect('usuarios:ingreso')
        
        else:
            # Hay errores en los datos.
            for error in form.errors.values():
                messages.info(request, error)    
    else:
        # Si no vengo de un post, vengo de un GET, lo unico que 
        # cambia es que tengo que armar un form vacío.
        form = FormRegistro()

    # El retorno es el mismo en cualquier caso.
    return render(request, "usuarios/registro.html", {'form': form})


# View de Ingreso de Usuario.
# Genero el form y lo presento al usuario y
# recibo por Metodo POST lo que hayan completado.
# Si se valida, lo autentifico y lo mando al HOME de la tienda.
@not_logged_required
def ingreso(request):
    # Si el request es POST, llega con los datos.
    if request.method == 'POST':

        # Autentifico al usuario.
        form = FormIngreso(request.POST)
        nxt = request.POST.get('next')

        uname = request.POST.get('username')
        pword = request.POST.get('password')

        user = authenticate(request, username=uname, password=pword)
        
        if not user:
            messages.info(request, "El usuario o la contraseña son incorrectos.")

        else:
            # Ingreso correcto.
            login(request, user)

            # Redirijo al home de la tienda.
            return HttpResponseRedirect(nxt)

    else:
        # Si no vengo de un post, vengo de un GET, lo unico que
        # cambia es que tengo que armar un form vacío.
        form = FormIngreso()

        # Si vengo redirigido de algun lado, guardo el enlace, 
        # de lo contrario, configuro el home de la tienda.
        nxt = request.GET.get('next')
        if not nxt:
            nxt = reverse('tienda:home')
        
    # El retorno es el mismo en cualquier caso.
    return render(request, "usuarios/login.html", {'form': form, 'next' : nxt})

def salida(request):

    # Desautentifico al usuario del request.    
    logout(request)

    # Redirijo al home
    return redirect('tienda:home')
