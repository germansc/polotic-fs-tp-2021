from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm


# Armo una nueva form de registro con más 
# campos para el registro.
class FormRegistro(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1',
                  'password2', 'first_name', 'last_name']
 
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        # Cambio las etiquetas.
        self.fields['username'].label = "Usuario"
        self.fields['email'].label = "Email"
        self.fields['first_name'].label = "Nombre"
        self.fields['last_name'].label = "Apellido"
        self.fields['password1'].label = "Contraseña"
        self.fields['password2'].label = "Confirmar Contraseña"

        # Fuerzo todo a requerido para el registro.
        self.fields['username'].required = True
        self.fields['email'].required = True
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['password1'].required = True
        self.fields['password2'].required = True

        # Agrego placeholders y una clase por si hay que estilizar.
        self.fields['username'].widget.attrs["class"] = "jag-text-form"
        self.fields['username'].widget.attrs["placeholder"] = self.fields['username'].label
        self.fields['email'].widget.attrs["class"] = "jag-text-form"
        self.fields['email'].widget.attrs["placeholder"] = self.fields['email'].label
        self.fields['first_name'].widget.attrs["class"] = "jag-text-form"
        self.fields['first_name'].widget.attrs["placeholder"] = self.fields['first_name'].label
        self.fields['last_name'].widget.attrs["class"] = "jag-text-form"
        self.fields['last_name'].widget.attrs["placeholder"] = self.fields['last_name'].label
        self.fields['password1'].widget.attrs["class"] = "jag-text-form"
        self.fields['password1'].widget.attrs["placeholder"] = self.fields['password1'].label
        self.fields['password2'].widget.attrs["class"] = "jag-text-form"
        self.fields['password2'].widget.attrs["placeholder"] = self.fields['password2'].label


# Armo una nueva form de login para acomodar los estilos y
# nombres de placeholder
class FormIngreso(AuthenticationForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Cambio las etiquetas.
        self.fields['username'].label = "Usuario"
        self.fields['password'].label = "Contraseña"

        # Agrego placeholders y una clase por si hay que estilizar.
        self.fields['username'].widget.attrs["class"] = "jag-text-form"
        self.fields['username'].widget.attrs["placeholder"] = self.fields['username'].label

        self.fields['password'].widget.attrs["class"] = "jag-text-form"
        self.fields['password'].widget.attrs["placeholder"] = self.fields['password'].label
