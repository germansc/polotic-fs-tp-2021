from django.shortcuts import redirect, render, reverse
from django.http import HttpResponseRedirect
from django.contrib import messages

# Decoradores.
from django.contrib.auth.decorators import login_required

# Modelos.
from django.db.models import Q
from .models import Categoria, Producto, Carro
from .forms import FormProducto

# Para busqueda recursiva con todas las palabras buscadas, 
# podria usar reduce.
from functools import reduce
import operator

# Funciones privadas de manejo de ayuda.
# Funcion de ayuda para todos los views que requieran obtener info del
# carro.
def obtenerCarroActual(user):
    # Obtener o crear el carro pendiente del usuario.
    try:
        carro = Carro.objects.get(usuario=user, pendiente=True)
    except:
        carro = None

    # En cualquier caso, retorno el carro.
    return carro


# Funcion para determinar si un usuario es moderador.
def esModerador(user):
    return user.groups.filter(name="moderador").exists()


# //////////////////////////////// VISTAS /////////////////////////////////// #

# View de la pantalla inicial - Home.
# Paso por contexto una lista con los últimos 10
# elementos ordenados de más a menos reciente.
def home(request):
    # Para actualizar modificar el Agregar/ir al carro, necesito
    # obtener el carro. Si no hay carro, el None tambien me sirve.
    carro = obtenerCarroActual(request.user)

    return render(request, "tienda/index.html", {
        'productos': Producto.objects.all().order_by('-id')[:10],
        'carro' : carro
    })


# View del sitio "Sobre Nosotros".
# Renderiza el template about.html que es simplemente
# un sitio estatico con info de la empresa.
def about(request):
    return render(request, "tienda/about.html")


# View del sitio de visualizacion de Producto.
def producto(request, prodId):
    
    #Antes que nada, redirijo a moderadores a "Editar Producto".
    if esModerador(request.user):
        return redirect('tienda:editar', prodId)
    
    # Si es cliente, sigo por acá.
    # Obtengo el producto identificado por el ID.
    try:
        prod = Producto.objects.get(pk=prodId)
    except:
        return render(request, "tienda/404.html", {
            "mensaje" : "Producto no encontrado",
        })

    # Para actualizar modificar el Agregar/ir al carro, necesito
    # obtener el carro. Si no hay carro, el None tambien me sirve.
    carro = obtenerCarroActual(request.user)

    # Si obtuve el producto.
    return render(request, "tienda/producto.html", {
        "producto" : prod,
        "carro" : carro
    })


# View para agregar o editar productos.
# Solo accesible para moderadores.
@login_required
def editarProducto(request, prodId=None):
    #Antes que nada, redirijo a clientes al "Home".
    if not esModerador(request.user):
        return redirect('tienda:home')
    
    # Si vengo de un POST, vengo con datos.
    if request.method == "POST":

        # Obtengo los campos del form, hay que hacer un pasaje previo con
        # prodId, porque en el campo del form obviamente se llena como 
        # Texto, en lugar de como un ID entero.
        prodstring = request.POST["prodId"]

        try:
            prodId = int(prodstring)
            inst = Producto.objects.get(id=prodId)
            mensaje = "Producto Modificado"
        except:
            # No se pudo convertir, probablemente el campo id era "None" y 
            # vengo de la pagina de agregar producto. Si la excepcion la tiro
            # objetcts.get, guardo como producto nuevo de todas formas.
            prodId = None
            inst = None
            mensaje = "Producto Agregado"

        # Ahora si, obtengo los campos del POST, y asocio con un objeto existente
        # si proId no era None.
        form = FormProducto(request.POST, request.FILES, instance=inst)

        # Verifico que la info sea valida, en cuyo caso, guardo el objeto
        # nuevo o modifico la instancia existente.
        if form.is_valid():
            form.save()
            messages.info(request, mensaje)

            # Redirijo al home.
            return redirect('tienda:home')

    # Si vengo de un GET, genero un form nuevo.
    else:
        if prodId is None:
            # Vengo de una solicitud de Nuevo producto, armo un
            # Form vacío.
            form = FormProducto()

        else:
            # Pasaron un ID, quiero editar un producto.
            # Obtengo el producto indicado.
            try:
                prod = Producto.objects.get(id=prodId)
            except:
                # Estan inventando o forzando urls, redirijo al home
                return redirect('tienda:home')

            # Id Valido, genero el form con los datos actuales del producto.
            form = FormProducto(instance=prod)

    return render(request, "tienda/editar_producto.html", {
        "form" : form,
        "prodId" : prodId,
    })


# View para eliminar Producto.
# Solo accesible para moderadores.
def eliminarProducto(request):
    # Antes que nada, verifico que el usuario sea moderador, y 
    # el método del request sea POST, para asegurarme que se 
    # llega por el boton de "Eliminar" producto en la página de 
    # edicion.
    if esModerador(request.user) and request.method == "POST":
        # Obtengo el ID del producto, que deberia llegar en un campo
        # oculto del request.
        prodId = request.POST["prodId"]
        
        # Si el campo es valido y el producto existe, lo elimino.
        if prodId and Producto.objects.filter(id=prodId).exists():
            Producto.objects.get(id=prodId).delete()
            messages.info(request, "Producto Eliminado")

    # En cualquier caso, finalmente redirijo al home.
    return redirect('tienda:home')


# View de la búsqueda de productos.
def busqueda(request, **kwargs):    
    # Chequeo que llegué aca desde la barra de busqueda con un POST.
    if request.method == 'POST':
        # Obtengo la frase buscada del request, y la convierto en una lista de 
        # palabras.
        palabras = request.POST['search-text'].split()

        # Chequeo por las dudas que no esté vacía.
        if not palabras:
            return HttpResponseRedirect(reverse("tienda:home"))

        # Hago una busqueda recursiva, donde tanto la descripcion o el nombre
        # tienen que contener todas las palabras buscadas.

        resultados = Producto.objects.filter(reduce(operator.and_, ((Q(nombre__contains=p) | Q(descr__contains=p)) for p in palabras)))

        # Retorno la pantalla de busqueda.
        return render(request, "tienda/search.html", {
            'frase' : ' '.join(palabras),
            'resultados' : resultados
        })

    else:
        # Request de GET, o sea que llego de los links de Categoria.
        
        # Obtengo el indice de la categoría. Si no se obtiene,  
        # no viene del link y redirijo al home.
        try:
            cat_id = Categoria.objects.get(nombre=kwargs['cat'])
        except:
            return HttpResponseRedirect(reverse("tienda:home"))

        # El proceso es similar, genero la lista de resultados a partir
        # de la categoria indicada en el link.
        resultados = Producto.objects.filter(categoria=cat_id)

        return render(request, "tienda/search.html", {
                          'categoria' : cat_id.nombre,
                          'resultados': resultados
                      })


# Vista de perfil de usuario.
# Esta vista podria presentar las preferencias del usuario, 
# lista de ordenes realizadas, y demás. Por lo pronto está fuera
# de los requerimientos, pero me sirve para darle un uso al "link"
# del nombre de usuario, y probar como funcionan los decoradores.
@login_required(login_url='usuarios:ingreso')
def perfil(request):
    return render(request, 'tienda/perfil.html')


# Agregar al carro.
# Esta view requiere un usuario logueado, al que asignar el carro, 
# Y no esta disponible para moderadores.
@login_required(login_url='usuarios:ingreso')
def agregarAlCarro(request, prodId, next):
    
    # Solo accesible para clientes, si algun moderador tipeó la direccion, 
    # lo redirecciono al home.
    if esModerador(request.user):
        return redirect('tienda:home')

    # Lo primero que hago es obtener o crear el carro pendiente 
    # del usuario.
    carroActual, status = Carro.objects.get_or_create(usuario=request.user, pendiente=True)

    # Obtengo el producto.
    p = Producto.objects.get(pk=prodId)

    # Agrego el producto al carro, y actualizo el total.
    carroActual.productos.add(p)
    carroActual.total = carroActual.listaTotal()
    carroActual.save()

    # Redirijo a la página de la que aprete el botón.
    messages.info(request, "Agregado al carro!")
    return HttpResponseRedirect(next)


# Funcion para sacar un producto del carro.
@login_required(login_url='tienda:home')
def sacarDelCarro(request, prodId):
    # Solo accesible para clientes, si algun moderador tipeó la direccion,
    # lo redirecciono al home.
    if esModerador(request.user):
        return redirect('tienda:home')
    
    # Si es cliente, continuo.
    carroActual = obtenerCarroActual(request.user)

    # Obtengo el producto.
    p = Producto.objects.get(pk=prodId)

    # Agrego el producto al carro, y actualizo el total.
    carroActual.productos.remove(p)
    carroActual.total = carroActual.listaTotal()
    carroActual.save()

    # Redirijo a la página de la que aprete el botón.
    return redirect('tienda:carro')


# Funcion para vaciar todo el carro.
@login_required(login_url='tienda:home')
def vaciarCarro(request):
    # Solo accesible para clientes, si algun moderador tipeó la direccion,
    # lo redirecciono al home.
    if esModerador(request.user):
        return redirect('tienda:home')

    # Obtengo el carro pendiente del usuario.
    carroActual = obtenerCarroActual(request.user)

    # Limpio todo el carro.
    carroActual.productos.clear()
    carroActual.total = carroActual.listaTotal()
    carroActual.save()

    # Redirijo a la página de la que aprete el botón.
    return redirect('tienda:carro')


# Vista del carro.
# View para generar la pantalla de carro, 
# requiere estar loggeado, por requerimiento por lo que 
# uso un decorador.
@login_required(login_url='usuarios:ingreso')
def verCarro(request):
    
    # Solo accesible para clientes, si algun moderador tipeó la direccion,
    # lo redirecciono al home.
    if esModerador(request.user):
        return redirect('tienda:home')

    # Obtengo el carro actual.
    carro = obtenerCarroActual(request.user)
    
    # Retorno el template pasando el carro por contexto.
    return render(request, 'tienda/carro.html', {
        "carro" : carro
    })


# Para hacer una limpieza creo este metodo en el que 
# antes de pasar a hacer el logout del usuario elimino 
# la orden pendiente, si existe.
def salir(request):
    # Obtengo y elimino el carro actual.
    carro = obtenerCarroActual(request.user)
    if carro:
        carro.delete()
    
    # redirijo al logout de usuarios.
    return redirect('usuarios:salida')
