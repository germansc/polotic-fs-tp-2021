from django import forms
from .models import Categoria, Producto

class FormProducto(forms.ModelForm):
    # Defino un nuevo campo "Nueva Categoria", por si la que corresponde no se
    # encuentra en la base de datos. Puede estar en blanco.
    nueva_cat = forms.CharField(max_length=32, label="Nueva Categoría")

    class Meta:
        model = Producto
        fields = '__all__'

    # Similar a con los usuarios, agrego placeholdes y clases por si
    # quiero estilizar luego.
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['nombre'].label = "Nombre del Producto"
        self.fields['imagen'].label = "Imagen"
        self.fields['descr'].label = "Descripción del producto"
        self.fields['precio'].label = "Precio"
        self.fields['categoria'].label = "Categoría"
        
        # Puedo dejar en blanco el form si agrego una nueva categoria.
        # Esto se revisa de todas formas en el clean().
        self.fields['categoria'].required = False
        self.fields['nueva_cat'].required = False
        
        self.fields['nueva_cat'].widget.attrs["class"] = "jag-text-form form-control"
        self.fields['nueva_cat'].widget.attrs["placeholder"] = "Agregar nueva categoría"

        self.fields['nombre'].widget.attrs["class"] = "jag-text-form form-control"
        self.fields['nombre'].widget.attrs["placeholder"] = self.fields['nombre'].label

        self.fields['imagen'].widget.attrs["class"] = "jag-text-form form-control"
        self.fields['imagen'].widget.initial_text = "Actual"
        self.fields['imagen'].widget.input_text = "Cambiar"
        self.fields['imagen'].widget.attrs["placeholder"] = self.fields['imagen'].label
        
        self.fields['descr'].widget.attrs["class"] = "jag-text-form form-control"
        self.fields['descr'].widget.attrs["placeholder"] = self.fields['descr'].label
        self.fields['precio'].widget.attrs["class"] = "jag-text-form form-control"
        self.fields['precio'].widget.attrs["placeholder"] = self.fields['precio'].label
        self.fields['categoria'].widget.attrs["class"] = "jag-text-form form-control"
        self.fields['categoria'].widget.attrs["placeholder"] = self.fields['categoria'].label

    # Método para validar el campo de cateogoría con los nuevos chequeos.
    def clean(self):
        # Limpio los campos iniciales.
        cleaned_data = super(FormProducto, self).clean()
        
        categoria = cleaned_data.get('categoria')
        nueva_cat = cleaned_data.get('nueva_cat')

        print(f"Values = Im' in...{categoria} {nueva_cat}")

        # Me aseguro que pasen solo uno de los dos campos.
        if (not (categoria or nueva_cat)) or (categoria and nueva_cat):
            raise forms.ValidationError({'categoria':["Complete uno de los campos de categoría.",]})
        elif not categoria:
            # Se agrego una nueva categoria.
            cat, status = Categoria.objects.get_or_create(nombre=nueva_cat)
            cleaned_data['categoria'] = cat
