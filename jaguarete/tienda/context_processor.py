from .models import Categoria

# Este método genera la lista de categorias para el 
# menú de navegacion incluido en el layout general.
# Y ademas una variable booleana "moderador" que indica
# si el usuario está en ese grupo.
def obtenerCategorias(request):
    return {
        'catList': Categoria.objects.all()
    }

def obtenerModStatus(request):
    esMod = request.user.groups.filter(name="moderador").exists()
    return {
        'moderador' : esMod,
    }
