from django.urls import path
from . import views

# En este archivo se definirán los linkeos a las views de la aplicación "Tienda".
app_name = "tienda"
urlpatterns = [
    path("", views.home, name="home"),
    path("nosotros/", views.about, name="about"),
    
    # Productos
    path("p/<int:prodId>", views.producto, name="producto"),

    # Para "nuevo" uso la misma view que editar, y decido internamente
    # como armar el form y los botones a presentar.
    path("p/editar/<int:prodId>", views.editarProducto, name="editar"),
    path("p/nuevo", views.editarProducto, name="nuevo"),
    path("p/eliminar", views.eliminarProducto, name="eliminar"),

    # Busqueda/Filtro
    path("busqueda/", views.busqueda, name="busqueda"),
    path("categoria/<str:cat>", views.busqueda, name="categorias"),
    
    # Usuario
    path("perfil/", views.perfil, name="perfil"),
    path("salir/", views.salir, name="salir"),
    
    # Carro
    path("agregar?P<int:prodId>&ref=<path:next>", views.agregarAlCarro, name="agregarAlCarro"),
    path("sacar?P<int:prodId>", views.sacarDelCarro, name="sacar"),
    path("vaciar", views.vaciarCarro, name="vaciar"),
    path("carro/", views.verCarro, name="carro"),
]
