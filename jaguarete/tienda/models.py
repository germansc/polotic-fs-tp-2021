from functools import total_ordering
from django.db import models
from django.contrib.auth.models import User
from django.db.models.deletion import SET_NULL, PROTECT

# Para localizacion de moneda.
import locale

# Todos los campos son obligatorios en los modelos. Si bien
# los valores de null y blank ya son False por defecto según 
# la documentacion, lo especifico a fin de que quede explicito.

# Modelo de categorías.
#   - nombre : Nombre de la Categoría.
#
class Categoria(models.Model):
    nombre = models.CharField(max_length=32, null=False, blank=False, unique=True)

    def __str__(self):
        return f"{self.nombre}"


# Modelo de Producto.
#   - nombre   : Nombre del producto.
#   - imagen   : Imágen demostrativa del producto.
#   - desc     : Descripción del producto.
#   - precio   : Precio del producto.
#   - categoria: Categoría a la que pertenece el producto.
#
class Producto(models.Model):
    nombre    = models.CharField(max_length=64, null=False, blank=False)
    imagen    = models.ImageField(upload_to='products', null=False, blank=False)
    descr     = models.TextField(max_length=512, null=False, blank=False)
    precio    = models.DecimalField(max_digits=11, decimal_places=2, null=False, blank=False)
    categoria = models.ForeignKey(Categoria, null=False, blank=False, on_delete=PROTECT, related_name="productos")

    def __str__(self):
        return f"{self.nombre.title()} ($ {self.precio:.2f})"

    # Esta método devuelve el primer párrafo de la descripcion.
    def descripcionCorta(self):
        return self.descr.split('\n')[0]

    # Este método devuelve un string con el precio en formato local.
    def precioLocal(self):
        locale.setlocale(locale.LC_ALL, '')
        return locale.format_string('$%.2f', self.precio, True)



# Modelo de Carro u Orden.
#   - usuario    : Usuario que realizo la compra.
#   - productos  : Lista de productos, uso un many-to-many para esto, ya que 
#                  obviamente quiero referenciar varios productos desde una 
#                  orden, pero si uso el modelo para guardar ordenes realizadas, 
#                  me puede interesar hacer una busqueda inversa, de todas las ordenes 
#                  que contengan determinado producto.
#   - total      : Precio total de los prouctos del carro. 
#   - fecha      : Fecha de generacion del carro. 
#   - pendiente  : Este flag me va a servir para distinguir entre ordenes finalizadas,
#                  y la orden que representa al carro actual del usuario. Idealmente 
#                 solo me interesa que se guarden las ordenes completadas, por lo que 
#                 se podria eliminar la orden pendiente al hacer logout.
#
class Carro(models.Model):
    usuario = models.ForeignKey(User, on_delete=SET_NULL, null=True, blank=False)
    productos = models.ManyToManyField(Producto, blank=True)
    total = models.DecimalField(max_digits=15, decimal_places=2, null=False, blank=False, default=0.0)
    fecha = models.DateTimeField(auto_now=True)
    pendiente = models.BooleanField(default=True)

    def listaProductos(self):
        return self.productos.all()

    def listaTotal(self):
        return sum([prod.precio for prod in self.productos.all()])

     # Este método devuelve un string con el precio en formato local.
    def totalLocal(self):
        locale.setlocale(locale.LC_ALL, '')
        return locale.format_string('$%.2f', self.total, True)

    def __str__(self):
        return f"Orden de {self.usuario.username} (Total ${self.listaTotal()})"
