# Requerimientos del Proyecto: 
##### Desarrollador: Germán Scillone | Cliente: Jaguareté KAA S.A. 

### Documentos de Referencia: 

**RF1** - "01 - Consigna del Trabajo Final 2021", PoloTIC Misiones 


### Descripción del Proyecto: 

La empresa Jaguareté JAA S.A. solicita el desarrollo de un sitio web para exponer y vender sus productos. El desarrollo a realizar se determina como fullstack, por lo que se deberá implementar tanto el backend (incluyendo base de datos) como el frontend del sitio. 

### REQUERIMIENTOS: 
#### Requerimientos de Diseño: 
- Todas las páginas del sitio deben ser homogéneas en su estilo. 

- El sitio debe implementar un diseño responsivo (debe poder visualizarse bien tanto en computadoras de escritorio como tablets o teléfonos celulares). 

#### Requerimientos de Estructura: 
- El sitio debe contar con 5 secciones: 
    - Encabezado 
    - Menú 
    - Contenido Principal 
    - Pie de página 
 
- El encabezado debe incorporar el logo de la empresa, y botones de acceso al registro de usuarios, logueo (o desconexión) de los mismos y al carro de compras. 

- El menú deberá tener un link al Home del sitio, un menú desplegable de categorías de productos, un link a la página de descripción de la empresa, un link de contacto y una barra de búsqueda de productos. 

- El Pie de página deberá contener el nombre de la empresa, copyright, y la información del desarrollador. 

- El Contenido Principal cambiará según la información que deba mostrar el sitio. 

- El sitio deberá contar por lo menos con las siguientes vistas: 
    - Pantalla de Inicio (Home, o pantalla de bienvenida). 
    - Pantalla de "Acerca de" con información de la empresa. 
    - Pantalla de Resultados de Búsqueda. 
    - Pantalla de Login. 
    - Pantalla de Registro. 
    - Pantalla de Producto. 
    - Pantalla de Carro. 
    - Pantalla de "Agregar Producto". 
    - Pantalla de "Editar Producto". 

- El cuerpo de la Pantalla de Inicio deberá presentar los últimos 10 productos agregados (mostrando las imágenes de los 3 más recientes), con links a las páginas de producto de los mismos. 

- El cuerpo de la pantalla de "Acerca de" debe ser un contenido estático que presente información de la empresa. 

- El cuerpo de la pantalla de registro deberá presentar un formulario que permita registrar al usuario con email, nombre de usuario, contraseña. Solo es accesible si el usuario no se encuentra logueado. 

- El cuerpo de la pantalla de Log In debe presentar un formulario para acceder al sitio web con nombre de usuario y contraseña. 

- El cuerpo de la pantalla de "Resultados de Búsqueda" debe listar los productos que coincidan con la búsqueda realizada (esto incluye el filtrado por categoría). 

- Los resultados de búsqueda compara los términos buscados con el título y la descripción de los productos. 

- El cuerpo de la pantalla del Producto debe presentar el nombre, descripción, categoría y una imagen del producto, así como su precio y un botón para agregar el producto al carro  
    - Si el usuario no está logueado, el botón de "Agregar al carro" deberá redirigir a pantalla de Log In. 

- Las pantallas de Editar o Agregar productos deberán ser solo accesibles para los moderadores. 

- El contenido de las pantallas de Editar o Agregar Productos deberá presentar un formulario que permita registrar Nombre de Producto, Categoría, Descripción, Precio y una imagen del mismo. También deberá presentar la opción de eliminar el producto del sitio. 

- El contenido de la pantalla de Carro debe listar los productos agregados al carrito, editar la cantidad, mostrar el valor total del mismo y un botón para finalizar la compra. 

#### Requerimientos de estilo: 
- El sitio debe implementar Bootstrap o algún framework similar. 

- En **RF1** se presentan estilos predeterminados para cada sección y pantalla del sitio. 

#### Requerimientos de Modelos: 
- Se deberán implementar al menos los siguientes modelos en el desarrollo de la aplicación: 
    - Modelo de Categorías 
        - Descripción 

    - Modelo de Productos 
        - Titulo 
        - Descripción 
        - Categoría 
        - Imagen 
        - Precio 

    - Modelo de Carro: 
        - Usuario 
        - Lista de Productos Agregados 
        - Total del Carro. 

- El modelo de usuario (y grupo "Moderador") deberá ser el mismo que implementa Django. 
    - La única diferencia entre estos será que los usuarios Moderadores podrán acceder a las pantallas de Editar Productos (en lugar de la página de producto tradicional) y Agregar Producto. 

- La base de datos deberá implementarse con SQLite. 

#### Requerimientos de Presentación: 

Extraídos directamente de **RF1** 
- Se deberá cumplir con al menos 70% de lo solicitado. 
- Se deberá subir el código fuente a un repositorio en alguna plataforma de hosting como GitLab o GitHub. 
- Se deberá grabar un video de como máximo 5 minutos mostrando las funcionalidades solicitadas (etiquetando en la descripción los instantes donde se presente cada funcionalidad). 
- Se deberá llenar un formulario de registro de entrega. 
- Se deberán haber aprobado 7 de las 10 evaluaciones del curso. 
- Fecha límite 7 de Julio de 2021. 
