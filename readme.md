## PoloTic: Curso de desarrollo Web Full Stack con Python y JavaScript
<hr>

### Trabajo Práctico 2021 
#### Germán Scillone

Este repositorio contiene la entrega del trabajo práctico del curso de 
desarrollo web FullStack 2021 de PoloTic Misiones. 

La estructura del repositorio se detalla a continuación:
- **assets:** *Recursos generados para el sitio, imágenes, logos, etc...*
- **doc:** *Documentación provista y generada sobre la aplicación a realizar.*
- **jaguarete:** *Directorio del proyecto Django.*
- **jaguarte/tienda:** *Directorio de la aplicación de django solicitada por el TP.*
- **jaguarte/usuarios:** *Directorio de la aplicación de django secundaria que se encarga del manejo de los usuarios.*
- **jaguarte/media:** *Directorio de archivos asociados a la base de datos del proyecto. En este caso solo contiene las imágenes de los productos.*

## Requisitos
-   Django v3.2 y sus dependencias.

<hr>

### Diseño
Se propuso el siguiente diseño para el sitio, a ser implementado siguiendo los principios *mobile-fist* con Bootstrap v5.0. Este diseño se ensayó hasta dispositivos de 320px de acho de pantalla que según los análisis realizados [es la resolución mínima de los viewports en pantallas móviles modernas](https://gbksoft.com/blog/common-screen-sizes-for-responsive-web-design/#toc-best-common-sizes-for-desktop-mobile-and-tablet-screens).

![image](assets/jag-kaa-design.svg)

Si bien podría configurarse y recompilarse el código Sass de Bootstrap
para implementar el estilo propuesto, se prevé simplemente sobreescribir
al mismo con un archivo CSS estático, a fin de practicar el lenguaje por
fuera del framework.

#### Consideraciones
Dado que no se especifica el rubro de la empresa ***Jaguareté KAA***, y 
teniendo en cuenta lo que tengo literalmente en frente, voy a considerar que es 
una empresa de venta de componentes de PC. De esta manera se pueden generar 
categorías y productos de placeholders un poco más representativos que 
"Categoria 1", "Categoria 2", "Producto X", etc.

<hr>

### Base de Datos Incluida:

Por defecto, el control de cambios en la base de datos lo tengo deshabilitado mediante  el `.gitignore`. Sin embargo, para facilitar la evaluación del sitio se incluye una base de datos pre cargada con los siguientes elementos:

#### Categorías 
    - Motherboards
    - Procesadores
    - RAM
    - Placas de Video
    - Almacenamiento

#### Productos
    - Se incluyen 10 productos distribuidos en las categorías mencionadas, con todos sus parámetros completos.

#### Usuarios
Se incluyen 2 usuarios principales para representar a los dos "tipos de usuarios" disponibles, y ambos tienen la misma contraseña "123" para facilitar el acceso.

    Usuario que pertenece al grupo de moderadores.
    - username: mod
    - password: 123

<br>

    Usuario que no pertenece al grupo de moderadores, representativo de cualquier
    usuario que se registre en el sitio.
    - username: cliente
    - password: 123

El manejo más fino de todos los modelos se deja en manos de un superusuario mediante la aplicación de administración de Django.
